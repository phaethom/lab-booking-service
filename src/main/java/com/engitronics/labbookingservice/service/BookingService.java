package com.engitronics.labbookingservice.service;

import com.engitronics.labbookingservice.model.Booking;
import com.engitronics.labbookingservice.repository.BookingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class BookingService  {

    @Autowired
    BookingRepository bookingRepository;

    public Page<Booking> retrieveAll(Pageable  pageable){
        return  bookingRepository.findAll(pageable);
    }

    public Optional<Booking> retrieveById(long id){
        return bookingRepository.findById(id);
    }
    public Booking createOrUpdate(Booking booking){
        return bookingRepository.save(booking);
    }

    public void deleteById(long id){
        bookingRepository.deleteById(id);
    }

}
