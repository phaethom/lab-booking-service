package com.engitronics.labbookingservice.service;

import com.engitronics.labbookingservice.model.Device;
import com.engitronics.labbookingservice.repository.DeviceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DeviceService {

    @Autowired
    DeviceRepository  deviceRepository;

    public Device createOrUpdate(Device device){
        return deviceRepository.save(device);
    }

    public List<Device> retrieveAll(){
       return deviceRepository.findAll();
    }

    public void deleteById(long id){
        deviceRepository.deleteById(id);
    }

    public  Optional<Device> retrieveById(long id){
        return deviceRepository.findById(id);
    }
}
