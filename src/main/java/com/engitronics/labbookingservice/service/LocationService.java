package com.engitronics.labbookingservice.service;

import com.engitronics.labbookingservice.model.Location;
import com.engitronics.labbookingservice.repository.LocationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class LocationService {

    @Autowired
    LocationRepository locationRepository;

    public List<Location> retrieveAll(){
        return locationRepository.findAll();
    }

    public Optional<Location> retrieveById(long id){
        return locationRepository.findById(id);
    }

    public void deleteById(long id){
        locationRepository.deleteById(id);
    }

    public Location createOrUpdate( Location location){
        return locationRepository.save(location);
    }
}
