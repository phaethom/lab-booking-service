package com.engitronics.labbookingservice.controller;


import com.engitronics.labbookingservice.exceptions.NotFoundException;
import com.engitronics.labbookingservice.model.Booking;
import com.engitronics.labbookingservice.service.BookingService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:3003")
public class BookingController {

    @Autowired
    BookingService bookingService;

    @ApiOperation(value="Create a new booking")
    @PostMapping("/booking")
    public ResponseEntity<Booking> createOrUpdate(@Valid @RequestBody Booking booking){
        Booking booking1 = bookingService.createOrUpdate(booking);
        return new ResponseEntity<Booking>(booking1, HttpStatus.CREATED);
    }

    @ApiOperation(value="Retrieve a single booking")
    @GetMapping("/booking/{id}")
    public ResponseEntity<Booking> retrieveById(@PathVariable long id){
        Booking booking = bookingService.retrieveById(id).orElseThrow(()-> new NotFoundException(String.format("Device with id %s not found!", id)));
        return new ResponseEntity<>(booking, HttpStatus.OK);
    }

    @ApiOperation(value="retrieve all bookings")
    @GetMapping("/bookings")
    public ResponseEntity<ArrayList<Booking>> retrieveAll(Pageable pageable){
        return new ResponseEntity(bookingService.retrieveAll(pageable), new HttpHeaders(),HttpStatus.OK);

    }

    @ApiOperation(value="Delete a single booking")
    @DeleteMapping("/booking/{id}")
    public ResponseEntity<String> deleteById(@PathVariable long id){
        bookingService.retrieveById(id).orElseThrow(() -> new NotFoundException(String.format("Booking with id %s not found!",id)));
        bookingService.deleteById(id);
        return new ResponseEntity<>("Deleted",HttpStatus.OK);
    }

    @ApiOperation(value="Update a single booking")
    @PutMapping("/booking/{id}")
    public ResponseEntity<Booking>  update(@PathVariable long id, @RequestBody @Valid Booking booking  ){
        bookingService.retrieveById(id).orElseThrow(() -> new NotFoundException(String.format("Booking with id %s not found!",id)));
        booking.setId(id);
        return new ResponseEntity<>(bookingService.createOrUpdate(booking),HttpStatus.OK);
    }
}
