package com.engitronics.labbookingservice.controller;


import com.engitronics.labbookingservice.exceptions.NotFoundException;
import com.engitronics.labbookingservice.model.Device;
import com.engitronics.labbookingservice.service.DeviceService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:3003")
public class DeviceController {

    @Autowired
    DeviceService deviceService;

    @ApiOperation(value="Create device")
    @PostMapping("/device")
    public ResponseEntity<Device> createOrUpdate(@RequestBody Device device, BindingResult result){
        Device device1 = deviceService.createOrUpdate(device);
        return new ResponseEntity<Device>(device1, HttpStatus.CREATED);
    }

    @ApiOperation(value="Retrieve all devices")
    @GetMapping("/devices")
    public ResponseEntity<ArrayList<Device>> retrieveAll(){
        return new ResponseEntity(deviceService.retrieveAll(), HttpStatus.OK);
    }

    @ApiOperation(value="Retrieve a single device")
    @GetMapping("/device/{id}")
    public ResponseEntity<Device> retrieveById(@PathVariable long id){
        Device device = deviceService.retrieveById(id).orElseThrow(() -> new NotFoundException(String.format("device with id %s not found!",id)));
        return new ResponseEntity<>(device,HttpStatus.OK);
    }

    @ApiOperation(value="Delete a single device")
    @DeleteMapping("device/{id}")
    public ResponseEntity<String> deleteById(@PathVariable long id){
        deviceService.retrieveById(id).orElseThrow(() -> new NotFoundException(String.format("Device with id %s not found!",id)));
        deviceService.deleteById(id);
        return new ResponseEntity<>("Deleted", HttpStatus.OK);
    }

    @ApiOperation(value="Update  single device")
    @PutMapping("/device")
    public Device update(@RequestBody  Device device){
        return deviceService.createOrUpdate(device);
    }
}
