package com.engitronics.labbookingservice.controller;

import com.engitronics.labbookingservice.exceptions.NotFoundException;
import com.engitronics.labbookingservice.model.Location;
import com.engitronics.labbookingservice.service.LocationService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:4000")
public class LocationController {

    @Autowired
    LocationService locationService;

    @ApiOperation(value="Create a new location")
    @PostMapping("/location")
    public ResponseEntity<Location> createOrUpdate(@Valid @RequestBody Location location){
        Location location1 = locationService.createOrUpdate(location);
        return new ResponseEntity<Location>(location1, HttpStatus.CREATED);
    }

    @ApiOperation(value="Retrieve locations")
    @GetMapping("/locations")
    public ResponseEntity<ArrayList<Location>> retrieveAll(){
        return new ResponseEntity(locationService.retrieveAll(), HttpStatus.OK);
    }

    @ApiOperation(value="Retrieve a single location ")
    @GetMapping("/location/{id}")
    public ResponseEntity<Location> retrieveById(@PathVariable long id){
     Location location = locationService.retrieveById(id).orElseThrow(() -> new NotFoundException(String.format("Location with id %s not found!",id)));
        return new ResponseEntity<>(location,HttpStatus.OK);
    }

    @ApiOperation(value="Delete a single location")
    @DeleteMapping("location/{id}")
    public ResponseEntity<String> deleteById(@PathVariable long id){
        locationService.retrieveById(id).orElseThrow(() -> new NotFoundException(String.format("Location with id %s not found!",id)));
        locationService.deleteById(id);
        return new ResponseEntity<>("Deleted", HttpStatus.OK);
    }

    @ApiOperation(value="update a single location")
    @PutMapping("/location/{id}")
    public ResponseEntity<Location> update(@PathVariable long id, @RequestBody @Valid Location location  ){
        locationService.retrieveById(id).orElseThrow(() -> new NotFoundException(String.format("Location with id %s not found!",id)));
        location.setId(id);
        return new ResponseEntity<>(locationService.createOrUpdate(location), HttpStatus.OK);
    }
}
