package com.engitronics.labbookingservice.repository;

import com.engitronics.labbookingservice.model.Booking;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BookingRepository extends PagingAndSortingRepository<Booking, Long> {

//    Page<Booking> retrieveAll(Pageable pageable);
//
//    Optional<Booking> retrieveById(long id);
//
//    Booking createOrUpdate(Booking booking);
//
//    void deleteById(long id);


}
