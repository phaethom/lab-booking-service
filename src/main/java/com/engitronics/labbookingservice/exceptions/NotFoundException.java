package com.engitronics.labbookingservice.exceptions;

import javassist.SerialVersionUID;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundException extends RuntimeException{

    private static final long SerialVersionUID = 1L;

    public NotFoundException(String message){
        super(message);
    }
}
