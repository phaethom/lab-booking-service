package com.engitronics.labbookingservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LabBookingServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(LabBookingServiceApplication.class, args);
	}

}
