package com.engitronics.labbookingservice.model;

import com.engitronics.labbookingservice.utils.Constants;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Table(name = "booking")
@Data
public class Booking {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private int deviceId;

    private String subject;

    private String description;

    @Column(name = "start_date")
   // @JsonFormat(pattern = Constants.LOCAL_DATE_TIME_FORMAT)
    private Date startDate;

    @Column(name = "end_date")
   // @JsonFormat(pattern = Constants.LOCAL_DATE_TIME_FORMAT)
    private Date endDate;

}
