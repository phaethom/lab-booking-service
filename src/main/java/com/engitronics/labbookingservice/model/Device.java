package com.engitronics.labbookingservice.model;

import com.engitronics.labbookingservice.utils.Constants;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;


@Entity
@Table(name = "device")
@Data
public class Device {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String mac;

    private String ipv4;
    private String ipv6;
    private int locationId;

    @JsonFormat(pattern = Constants.LOCAL_DATE_TIME_FORMAT)
    @Column(name = "created_at")
    private Date createdAt;

    @PrePersist
    protected void onCreate(){
        this.createdAt = new Date();
    }

}
