# README #

## Lab Booking system

### Backend service for Lab Booking

* Version 1.0

#### High level service diagram

![Diagram](./lab.jpg)


### Requirement

 ![Docker](https://docs.docker.com/get-docker/)

![MySQL](https://www.mysql.com/)

![Keycloak](https://www.keycloak.org/)


### Running the application

#### Can use any idea of your preference or by command line

### command line Runner

Inside root directory type:

* gradle wrapper
* gradle bootRun

### Documentation
2. Access the swagger documentation describing the API and all the expected request datatype and more
   Certified that the application it's running
    *  Go to your browser and type:
        * http://localhost:8083/swagger-ui.html
          [swagger documentation](http://localhost:8087/swagger-ui.html)


### Authors
Odair Pires