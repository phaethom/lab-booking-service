
SET GLOBAL default_storage_engine = 'InnoDB';

create table if NOT EXISTS location(
id INT(2) NOT NULL AUTO_INCREMENT,
name VARCHAR(20),
PRIMARY KEY (id)
)engine=innodb;

create table if not EXISTS device(
id INT(2) NOT NULL AUTO_INCREMENT PRIMARY KEY,
 location_id INT(2) NOT NULL,
 name VARCHAR(30) NOT NULL,
 ipv4 VARCHAR(15) UNIQUE,
 ipv6 VARCHAR(39) UNIQUE,
 mac VARCHAR(18) UNIQUE,
 FOREIGN KEY (location_id) REFERENCES location(id),
 created_at DATETIME
)engine=innodb;

create table if NOT EXISTS booking(
id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
subject VARCHAR(100),
description VARCHAR(200),
device_id INT(2) NOT NULL,
start_date DATETIME NOT NULL,
end_date DATETIME NOT NULL,
FOREIGN KEY (device_id) REFERENCES device (id)
)engine=innodb;